<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use App\Entity\Product\BoardGame;
use App\Entity\Product\Book;
use App\Entity\Product\Dice;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    const BRAND_NUMBER = 10;

    const PRODUCT_NUMBER = 10;

    /**
     * @var ObjectManager
     */
    protected $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        for ($i = 0; $i < self::BRAND_NUMBER; $i++) {
            $brand = $this->createBrand($i + 1);
            $manager->persist($brand);
        }

        $manager->flush();

        for ($i = 0; $i < self::PRODUCT_NUMBER; $i++) {
            $boardGame = $this->createBoardGames($i + 1);
            $manager->persist($boardGame);
            $book = $this->createBook($i + 1);
            $manager->persist($book);
            $dice = $this->createDice($i + 1);
            $manager->persist($dice);
        }

        $manager->flush();
    }

    private function createBrand(int $counter): Brand
    {
        $brand = new Brand();
        $brand->setName("Brand-" . $counter);
        $brand->setQuality(rand(1, 5));
        return $brand;
    }

    private function getRandomBrand(): Brand
    {
        /** @var Brand[] $brands */
        $brands = $this->manager->getRepository('App:Brand')->findAll();
        return $brands[rand(0, count($brands) - 1)];
    }

    private function createBoardGames(int $counter): BoardGame
    {
        $boardGame = new BoardGame();
        $boardGame->setName('BoardGame-' . $counter);
        $boardGame->setPrice(rand(6000, 2500));
        $boardGame->setSku('BoardGame-SKU-' . $counter);
        $boardGame->setAuthor('Author-' . $counter);
        $boardGame->setComplexity(rand(1, 5));
        $boardGame->setBrand($this->getRandomBrand());

        return $boardGame;
    }

    private function createDice(int $counter): Dice
    {
        $colors = ['red', 'green', 'blue', 'white', 'black', 'yellow'];
        $sides = [4, 6, 8, 10, 12, 20];
        $dice = new Dice();
        $dice->setName('Dice-' . $counter);
        $dice->setPrice(rand(100, 500));
        $dice->setSku('Dice-SKU-' . $counter);
        $dice->setSide($sides[rand(0, 5)]);
        $dice->setColor($colors[rand(0, 5)]);
        $dice->setBrand($this->getRandomBrand());

        return $dice;
    }

    private function createBook(int $counter): Book
    {
        $types = ["fantasy", "sci-fi", "romantic", "adventure", "crime", "tale"];
        $book = new Book();
        $book->setName('Book-' . $counter);
        $book->setPrice(rand(1500, 5000));
        $book->setSku('Book-SKU-' . $counter);
        $book->setAuthor('Author-' . $counter);
        $book->setType($types[rand(0, 5)]);
        $book->setBrand($this->getRandomBrand());

        return $book;
    }
}
