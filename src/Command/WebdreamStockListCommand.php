<?php

namespace App\Command;

use App\Entity\AbstractProduct;
use App\Entity\Stock;
use App\Handler\StockHandler;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WebdreamStockListCommand extends Command
{
    protected static $defaultName = 'webdream:stock:list';

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * WebdreamStockCreateCommand constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->manager = $container->get('doctrine')->getManager();
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('StockName', InputArgument::OPTIONAL, 'Stock name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $stockHandler = new StockHandler($this->manager);

        $stockName = $input->getArgument('StockName');
        if (isset($stockName)) {
            /** @var Stock $stock */
            $stock = $stockHandler->getStock($stockName);
            $output->writeln(
                sprintf(
                    '%s stock has %s piece of product',
                    $stock->getName(),
                    $stockHandler->countActualProduct($stock)
                )
            );
            /** @var AbstractProduct $product */
            foreach ($stock->getProductCollection() as $product) {
                $qty = $product['qty'];
                switch ($product['type']) {
                    case "App\Entity\Product\BoardGame":
                        $product = $stockHandler->getBoardGameById($product['product']);
                        $output->writeln(
                            sprintf(
                                'Brand: %s | Name: %s | Sku: %s | Price: %s | Author: %s | Complexity: %s | Qty: %s',
                                $stockHandler->getBrandOfProduct($product)->getName(),
                                $product->getName(),
                                $product->getSku(),
                                $product->getPrice(),
                                $product->getAuthor(),
                                $product->getComplexity(),
                                $qty
                            )
                        );
                        break;
                    case "App\Entity\Product\Book":
                        $product = $stockHandler->getBookById($product['product']);
                        $output->writeln(
                            sprintf(
                                'Brand: %s | Name: %s | Sku: %s | Price: %s | Author: %s | Type: %s | Qty: %s',
                                $stockHandler->getBrandOfProduct($product)->getName(),
                                $product->getName(),
                                $product->getSku(),
                                $product->getPrice(),
                                $product->getAuthor(),
                                $product->getType(),
                                $qty
                            )
                        );
                        break;
                    case "App\Entity\Product\Dice":
                        $product = $stockHandler->getDiceById($product['product']);
                        $output->writeln(
                            sprintf(
                                'Brand: %s | Name: %s | Sku: %s | Price: %s | Side: %s | Color: %s | Qty: %s',
                                $stockHandler->getBrandOfProduct($product)->getName(),
                                $product->getName(),
                                $product->getSku(),
                                $product->getPrice(),
                                $product->getSide(),
                                $product->getColor(),
                                $qty
                            )
                        );
                        break;
                }
            }
        } else {
            foreach ($stockHandler->getStocks() as $stock) {
                $output->writeln(
                    sprintf(
                        'name: %s | address: %s | capacity: %s | actual: %s',
                        $stock->getName(),
                        $stock->getAddress(),
                        $stock->getCapacity(),
                        $stockHandler->countActualProduct($stock)
                    )
                );
            }
            $io->success('You can list specific stock if you write a stock name after this command');
        }

        return Command::SUCCESS;
    }
}
