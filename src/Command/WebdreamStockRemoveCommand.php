<?php

namespace App\Command;

use App\Entity\Stock;
use App\Handler\StockHandler;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WebdreamStockRemoveCommand extends Command
{
    protected static $defaultName = 'webdream:stock:remove';

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * WebdreamStockCreateCommand constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->manager = $container->get('doctrine')->getManager();
        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('StockName', InputArgument::OPTIONAL, 'Stock name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $stockHandler = new StockHandler($this->manager);

        $stockName = $input->getArgument('StockName');

        $stockHandler->removeStock($stockName);

        $io->success($stockName . ' stock removed');
        return Command::SUCCESS;
    }
}
