<?php

namespace App\Command;

use App\Handler\StockHandler;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WebdreamProductAddCommand extends Command
{
    protected static $defaultName = 'webdream:product:add';

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * WebdreamStockCreateCommand constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->manager = $container->get('doctrine')->getManager();
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('sku', InputArgument::OPTIONAL, 'Product sku')
            ->addArgument('qty', InputArgument::OPTIONAL, 'Product quantity');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $sku = $input->getArgument('sku');
        $qty = $input->getArgument('qty');

        $stockHandler = new StockHandler($this->manager);

        $stockHandler->addProduct($stockHandler->getProductBySku($sku), $qty);

        $io->success('Products in in stock');
        return 0;
    }
}
