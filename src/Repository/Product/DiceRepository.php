<?php

namespace App\Repository\Product;

use App\Entity\Product\Dice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Dice|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dice|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dice[]    findAll()
 * @method Dice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dice::class);
    }
}
