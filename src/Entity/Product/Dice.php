<?php

namespace App\Entity\Product;

use App\Entity\AbstractProduct;
use App\Repository\Product\DiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DiceRepository::class)
 */
class Dice extends AbstractProduct
{
    /**
     * @ORM\Column(type="integer")
     */
    private $side;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $color;

    public function getSide(): ?int
    {
        return $this->side;
    }

    public function setSide(int $side): self
    {
        $this->side = $side;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
