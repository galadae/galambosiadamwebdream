<?php

namespace App\Entity\Product;

use App\Entity\AbstractProduct;
use App\Repository\Product\BoardGameRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=BoardgameRepository::class)
 */
class BoardGame extends AbstractProduct
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $author;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min=1,max=5,notInRangeMessage="Complexity has to be beetween {{ min }} and {{ max }}")
     */
    private $complexity;

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getComplexity(): ?int
    {
        return $this->complexity;
    }

    public function setComplexity(int $complexity): self
    {
        $this->complexity = $complexity;

        return $this;
    }
}
