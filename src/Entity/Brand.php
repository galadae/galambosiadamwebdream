<?php

namespace App\Entity;

use App\Repository\BrandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=BrandRepository::class)
 */
class Brand
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min=1,max=5,notInRangeMessage="Complexity has to be beetween {{ min }} and {{ max }}")
     */
    private $quality;

    /**
     * @OneToMany(targetEntity="AbstractProduct", mappedBy="brand", cascade={"persist", "remove"})
     */
    private $products;

    /**
     * Brand constructor.
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getQuality(): ?int
    {
        return $this->quality;
    }

    public function setQuality(int $quality): self
    {
        $this->quality = $quality;

        return $this;
    }

    public function getProducts(): ?ArrayCollection
    {
        return $this->products;
    }

    public function setProducts(ArrayCollection $products): self
    {
        $this->products = $products;

        return $this;
    }

    public function addProduct(AbstractProduct $product): self
    {
        $this->products->add($product);

        return $this;
    }

    public function removeProduct(AbstractProduct $product): self
    {
        $this->products->remove($product->getId());

        return $this;
    }
}
