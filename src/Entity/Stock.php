<?php

namespace App\Entity;

use App\Repository\StockRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StockRepository::class)
 */
class Stock
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacity;

    /**
     * @ORM\Column(type="string", length=225)
     */
    private $productCollection;

    /**
     * Stock constructor.
     */
    public function __construct()
    {
        $this->productCollection = serialize([]);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getProductCollection(): array
    {
        return unserialize($this->productCollection);
    }

    public function setProductCollection(array $productCollection): self
    {
        $this->productCollection = serialize($productCollection);

        return $this;
    }

    public function addProduct(AbstractProduct $product, $qty = 1): self
    {
        $stock = $this->getProductCollection();
        for ($i = 0; $i < count($stock); $i++) {
            if ($stock[$i]['product'] == $product->getId() && $stock[$i]['type'] == get_class($product)) {
                $stock[$i]['qty'] += (int)$qty;
                return $this->setProductCollection($stock);
            }
        }
        $stock[] = ['product' => $product->getId(), 'qty' => $qty, 'type' => get_class($product)];
        return $this->setProductCollection($stock);
    }

    public function removeProduct(AbstractProduct $product, $qty = 1): int
    {
        $stock = $this->getProductCollection();
        $left = $qty;
        for ($i = 0; $i < count($stock); $i++) {
            if ($stock[$i]['product'] == $product->getId() && $stock[$i]['type'] == get_class($product)) {
                if ($stock[$i]['qty'] > (int)$left) {
                    $stock[$i]['qty'] -= (int)$left;
                    $left = 0;
                } else {
                    $left -= $stock[$i]['qty'];
                    $stock[$i]['qty'] -= (int)$left;
                    unset($stock[$i]);
                }
            }
        }
        $this->setProductCollection($stock);
        return $left;
    }

}
