<?php

namespace App\Handler;

use App\Entity\AbstractProduct;
use App\Entity\Product\BoardGame;
use App\Entity\Product\Book;
use App\Entity\Product\Dice;
use App\Entity\Stock;
use Doctrine\Persistence\ObjectManager;
use Exception;

class StockHandler
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    public function createStock($name, $capacity, $address)
    {
        $stock = new Stock();
        $stock->setName($name);
        $stock->setCapacity($capacity);
        $stock->setAddress($address);

        $this->manager->persist($stock);
        $this->manager->flush();
    }

    public function removeStock($name)
    {
        $stock = $this->getStockRepository()->findOneBy(['name' => $name]);

        $this->manager->remove($stock);
        $this->manager->flush();
    }

    public function removeAllStock()
    {
        $stocks = $this->getStockRepository()->findAll();

        foreach ($stocks as $stock) {
            $this->manager->remove($stock);
        }
        $this->manager->flush();
    }

    /**
     * @return Stock[]
     */
    public function getStocks(): array
    {
        return $this->getStockRepository()->findAll();
    }

    public function getStock($stockName)
    {
        return $this->getStockRepository()->findOneBy(['name' => $stockName]);
    }

    public function getBrandOfProduct(AbstractProduct $product)
    {
        return $this->manager->getRepository('App:Brand')->find($product->getBrand());
    }

    /**
     * @param $sku
     * @return AbstractProduct|null
     * @throws Exception
     */
    public function getProductBySku($sku)
    {
        /** @var BoardGame $found */
        $found = $this->manager->getRepository('App:Product\BoardGame')->findOneBy(['sku' => $sku]);
        if (!empty($found)) {
            return $found;
        }
        /** @var Book $found */
        $found = $this->manager->getRepository('App:Product\Book')->findOneBy(['sku' => $sku]);
        if (!empty($found)) {
            return $found;
        }
        /** @var Dice $found */
        $found = $this->manager->getRepository('App:Product\Dice')->findOneBy(['sku' => $sku]);
        if (!empty($found)) {
            return $found;
        }
        throw new Exception('Sku not found');
    }

    /**
     * @param AbstractProduct $product
     * @param int $qty
     * @return bool
     * @throws Exception
     */
    public function addProduct(AbstractProduct $product, $qty): bool
    {
        $addMore = $qty;
        $stocks = $this->getStockRepository()->findAll();
        $i = 0;
        while ($i < count($stocks)) {
            /** @var Stock $stock */
            $stock = $stocks[$i];
            if ($stock->getCapacity() > count($stock->getProductCollection())) {
                $diff = $this->getAvailableCapacity($stock);
                $canAdd = $diff > $addMore ? $addMore : $diff;
                $stock->addProduct($product, $canAdd);
                $addMore -= $canAdd;
                $this->manager->persist($stock);
                $this->manager->flush();
                if ($addMore === 0) {
                    return true;
                }
            }
            $i++;
        }
        throw new Exception("Had not enough space in stocks. " . $addMore . " product left");
    }

    /**
     * @param AbstractProduct $product
     * @param int $qty
     * @return bool
     * @throws Exception
     */
    public function removeProduct(AbstractProduct $product, $qty): bool
    {
        $removeMore = $qty;
        $stocks = $this->getStockRepository()->findAll();
        $i = 0;
        while ($i < count($stocks)) {
            /** @var Stock $stock */
            $stock = $stocks[$i];
            $removeMore = $stock->removeProduct($product, $removeMore);
            $this->manager->persist($stock);
            $this->manager->flush();
            if ($removeMore === 0) {
                return true;
            }
            $i++;
        }
        throw new Exception("Wasn't enough products in stocks. Couldn't remove " . $removeMore . " products");
    }


    public function countActualProduct(Stock $stock)
    {
        $counter = 0;
        foreach ($stock->getProductCollection() as $item) {
            $counter += $item['qty'];
        }
        return $counter;
    }

    public function getAvailableCapacity(Stock $stock)
    {
        return $stock->getCapacity() - $this->countActualProduct($stock);
    }

    public function getBoardGameById($id)
    {
        return $this->manager->getRepository('App:Product\BoardGame')->find($id);
    }

    public function getBookById($id)
    {
        return $this->manager->getRepository('App:Product\Book')->find($id);
    }

    public function getDiceById($id)
    {
        return $this->manager->getRepository('App:Product\Dice')->find($id);
    }

    protected function getStockRepository()
    {
        return $this->manager->getRepository('App:Stock');
    }
}