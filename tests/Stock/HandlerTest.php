<?php

namespace App\Tests\Util;

use App\Entity\AbstractProduct;
use App\Entity\Stock;
use App\Handler\StockHandler;
use Doctrine\ORM\EntityManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HandlerTest extends KernelTestCase
{
    const SINGLE_STOCK_NAME = "Stock";
    const MULTI_STOCK_NAME_1 = "Stock1";
    const MULTI_STOCK_NAME_2 = "Stock2";

    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var StockHandler
     */
    private $stockHandler;

    /**
     * @var AbstractProduct
     * This test product is in the fixtures with exactly this parameters
     * In real environment this test product should created by test cases
     * name: BoardGame-1
     * sku: BoardGame-SKU-1
     * author: author-1
     * Other data is random, but tests not checks data directly
     */
    private $testProduct1;

    /**
     * @var AbstractProduct
     * This test product is in the fixtures with exactly this parameters
     * In real environment this test product should created by test cases
     * name: Dice-1
     * sku: Dice-SKU-1
     * Other data is random, but tests not checks data directly
     */
    private $testProduct2;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->manager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->stockHandler = new StockHandler($this->manager);
        $this->stockHandler->removeAllStock();

        $this->testProduct1 = $this->stockHandler->getProductBySku('BoardGame-SKU-1');
        $this->testProduct2 = $this->stockHandler->getProductBySku('Dice-SKU-1');
    }

    public function testCreateStock()
    {
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_1, 10, 'Test');
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_2, 11, 'Address');
        /** @var Stock $testStock */
        $testStock = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_1);
        $this->assertEquals(self::MULTI_STOCK_NAME_1, $testStock->getName());
        $this->assertEquals(10, $testStock->getCapacity());
        $this->assertEquals('Test', $testStock->getAddress());

        $testStock = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_2);
        $this->assertEquals(self::MULTI_STOCK_NAME_2, $testStock->getName());
        $this->assertEquals(11, $testStock->getCapacity());
        $this->assertEquals('Address', $testStock->getAddress());
    }

    public function testDuplicatedStockName()
    {
        try {
            $this->stockHandler->createStock(self::SINGLE_STOCK_NAME, 10, 'Test');
            $this->stockHandler->createStock(self::SINGLE_STOCK_NAME, 10, 'Test');
        } catch (Exception $exception) {
            $this->assertContains(
                "SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry '" . self::SINGLE_STOCK_NAME . "'",
                $exception->getMessage()
            );
        }
    }

    public function testAddSingleProductToSingleStockSuccessful()
    {
        $this->stockHandler->createStock(self::SINGLE_STOCK_NAME, 10, 'Test');
        $result = $this->stockHandler->addProduct($this->testProduct1, 6);

        /** @var Stock $stock */
        $stock = $this->stockHandler->getStock(self::SINGLE_STOCK_NAME);

        $this->assertEquals(true, $result);
        $this->assertEquals(4, $this->stockHandler->getAvailableCapacity($stock));
        $this->assertEquals(6, $this->stockHandler->countActualProduct($stock));
    }

    public function testAddSingleProductToSingleStockFail()
    {
        $this->stockHandler->createStock(self::SINGLE_STOCK_NAME, 10, 'Test');
        try {
            $this->stockHandler->addProduct($this->testProduct1, 11);
        } catch (Exception $exception) {
            /** @var Stock $stock */
            $stock = $this->stockHandler->getStock(self::SINGLE_STOCK_NAME);

            $this->assertEquals(0, $this->stockHandler->getAvailableCapacity($stock));
            $this->assertEquals(10, $this->stockHandler->countActualProduct($stock));
            $this->assertEquals('Had not enough space in stocks. 1 product left', $exception->getMessage());
        }
    }

    public function testAddSingleProductToMultiStockSuccessful()
    {
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_1, 10, 'Test');
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_2, 10, 'Test');
        $result = $this->stockHandler->addProduct($this->testProduct1, 15);

        /** @var Stock $stock1 */
        $stock1 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_1);
        /** @var Stock $stock2 */
        $stock2 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_2);

        $this->assertEquals(true, $result);
        $this->assertEquals(0, $this->stockHandler->getAvailableCapacity($stock1));
        $this->assertEquals(10, $this->stockHandler->countActualProduct($stock1));
        $this->assertEquals(5, $this->stockHandler->getAvailableCapacity($stock2));
        $this->assertEquals(5, $this->stockHandler->countActualProduct($stock2));
    }

    public function testAddSingleProductToMultiStockFail()
    {
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_1, 10, 'Test');
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_2, 10, 'Test');

        try {
            $this->stockHandler->addProduct($this->testProduct1, 25);
        } catch (Exception $exception) {
            /** @var Stock $stock1 */
            $stock1 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_1);
            /** @var Stock $stock2 */
            $stock2 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_2);

            $this->assertEquals(0, $this->stockHandler->getAvailableCapacity($stock1));
            $this->assertEquals(10, $this->stockHandler->countActualProduct($stock1));
            $this->assertEquals(0, $this->stockHandler->getAvailableCapacity($stock2));
            $this->assertEquals(10, $this->stockHandler->countActualProduct($stock2));
            $this->assertEquals('Had not enough space in stocks. 5 product left', $exception->getMessage());
        }

    }

    public function testAddMultiProductToSingleStockSuccessful()
    {
        $this->stockHandler->createStock(self::SINGLE_STOCK_NAME, 10, 'Test');
        $result1 = $this->stockHandler->addProduct($this->testProduct1, 3);
        $result2 = $this->stockHandler->addProduct($this->testProduct2, 3);

        /** @var Stock $stock */
        $stock = $this->stockHandler->getStock(self::SINGLE_STOCK_NAME);

        $this->assertEquals(true, $result1);
        $this->assertEquals(true, $result2);
        $this->assertEquals(4, $this->stockHandler->getAvailableCapacity($stock));
        $this->assertEquals(6, $this->stockHandler->countActualProduct($stock));
    }

    public function testAddMultiProductToSingleStockFail()
    {
        $this->stockHandler->createStock(self::SINGLE_STOCK_NAME, 10, 'Test');

        try {
            $result1 = $this->stockHandler->addProduct($this->testProduct1, 3);
            $result2 = $this->stockHandler->addProduct($this->testProduct2, 9);
        } catch (Exception $exception) {
            /** @var Stock $stock */
            $stock = $this->stockHandler->getStock(self::SINGLE_STOCK_NAME);

            $this->assertEquals(0, $this->stockHandler->getAvailableCapacity($stock));
            $this->assertEquals(10, $this->stockHandler->countActualProduct($stock));

            $this->assertEquals('Had not enough space in stocks. 2 product left', $exception->getMessage());
        }

    }

    public function testAddMultiProductToMultiStockSuccessful()
    {
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_1, 10, 'Test');
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_2, 10, 'Test');
        $result1 = $this->stockHandler->addProduct($this->testProduct1, 9);
        $result2 = $this->stockHandler->addProduct($this->testProduct1, 4);

        /** @var Stock $stock1 */
        $stock1 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_1);
        /** @var Stock $stock2 */
        $stock2 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_2);

        $this->assertEquals(true, $result1);
        $this->assertEquals(true, $result2);
        $this->assertEquals(0, $this->stockHandler->getAvailableCapacity($stock1));
        $this->assertEquals(10, $this->stockHandler->countActualProduct($stock1));
        $this->assertEquals(7, $this->stockHandler->getAvailableCapacity($stock2));
        $this->assertEquals(3, $this->stockHandler->countActualProduct($stock2));
    }

    public function testAddMultiProductToMultiStockFail()
    {
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_1, 10, 'Test');
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_2, 10, 'Test');
        try {
            $result1 = $this->stockHandler->addProduct($this->testProduct1, 9);
            $result2 = $this->stockHandler->addProduct($this->testProduct1, 14);
        } catch (Exception $exception) {
            /** @var Stock $stock1 */
            $stock1 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_1);
            /** @var Stock $stock2 */
            $stock2 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_2);

            $this->assertEquals(0, $this->stockHandler->getAvailableCapacity($stock1));
            $this->assertEquals(10, $this->stockHandler->countActualProduct($stock1));
            $this->assertEquals(0, $this->stockHandler->getAvailableCapacity($stock2));
            $this->assertEquals(10, $this->stockHandler->countActualProduct($stock2));

            $this->assertEquals('Had not enough space in stocks. 3 product left', $exception->getMessage());
        }
    }

    public function testRemoveSingleProductFromSingleStockSuccessful()
    {
        $this->stockHandler->createStock(self::SINGLE_STOCK_NAME, 10, 'Test');
        $this->stockHandler->addProduct($this->testProduct1, 9);

        $this->stockHandler->removeProduct($this->testProduct1, 5);

        /** @var Stock $stock */
        $stock = $this->stockHandler->getStock(self::SINGLE_STOCK_NAME);

        $this->assertEquals(6, $this->stockHandler->getAvailableCapacity($stock));
        $this->assertEquals(4, $this->stockHandler->countActualProduct($stock));
    }

    public function testRemoveSingleProductFromSingleStockFail()
    {
        $this->stockHandler->createStock(self::SINGLE_STOCK_NAME, 10, 'Test');
        $this->stockHandler->addProduct($this->testProduct1, 5);

        try {
            $this->stockHandler->removeProduct($this->testProduct1, 9);
        } catch (Exception $exception) {
            /** @var Stock $stock */
            $stock = $this->stockHandler->getStock(self::SINGLE_STOCK_NAME);

            $this->assertEquals(10, $this->stockHandler->getAvailableCapacity($stock));
            $this->assertEquals(0, $this->stockHandler->countActualProduct($stock));
            $this->assertEquals("Wasn't enough products in stocks. Couldn't remove 4 products", $exception->getMessage());
        }
    }

    public function testRemoveSingleProductFromMultiStockSuccessful()
    {
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_1, 10, 'Test');
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_2, 10, 'Test');
        $this->stockHandler->addProduct($this->testProduct1, 15);

        $this->stockHandler->removeProduct($this->testProduct1, 9);

        /** @var Stock $stock1 */
        $stock1 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_1);
        /** @var Stock $stock2 */
        $stock2 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_2);

        $this->assertEquals(9, $this->stockHandler->getAvailableCapacity($stock1));
        $this->assertEquals(1, $this->stockHandler->countActualProduct($stock1));
        $this->assertEquals(5, $this->stockHandler->getAvailableCapacity($stock2));
        $this->assertEquals(5, $this->stockHandler->countActualProduct($stock2));
    }

    public function testRemoveSingleProductFromMultiStockFail()
    {
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_1, 10, 'Test');
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_2, 10, 'Test');
        $this->stockHandler->addProduct($this->testProduct1, 15);

        try {
            $this->stockHandler->removeProduct($this->testProduct1, 20);
        } catch (Exception $exception) {
            /** @var Stock $stock1 */
            $stock1 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_1);
            /** @var Stock $stock2 */
            $stock2 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_2);

            $this->assertEquals(10, $this->stockHandler->getAvailableCapacity($stock1));
            $this->assertEquals(0, $this->stockHandler->countActualProduct($stock1));
            $this->assertEquals(10, $this->stockHandler->getAvailableCapacity($stock2));
            $this->assertEquals(0, $this->stockHandler->countActualProduct($stock2));
            $this->assertEquals("Wasn't enough products in stocks. Couldn't remove 5 products", $exception->getMessage());
        }
    }

    public function testRemoveMultiProductFromSingleStockSuccessful()
    {
        $this->stockHandler->createStock(self::SINGLE_STOCK_NAME, 10, 'Test');
        $this->stockHandler->addProduct($this->testProduct1, 6);
        $this->stockHandler->addProduct($this->testProduct2, 2);

        $this->stockHandler->removeProduct($this->testProduct1, 5);
        $this->stockHandler->removeProduct($this->testProduct2, 1);

        /** @var Stock $stock1 */
        $stock1 = $this->stockHandler->getStock(self::SINGLE_STOCK_NAME);

        $this->assertEquals(8, $this->stockHandler->getAvailableCapacity($stock1));
        $this->assertEquals(2, $this->stockHandler->countActualProduct($stock1));
    }

    public function testRemoveMultiProductFromSingleStockFail()
    {
        $this->stockHandler->createStock(self::SINGLE_STOCK_NAME, 10, 'Test');
        $this->stockHandler->addProduct($this->testProduct1, 6);
        $this->stockHandler->addProduct($this->testProduct2, 2);

        try {
            $this->stockHandler->removeProduct($this->testProduct1, 5);
            $this->stockHandler->removeProduct($this->testProduct2, 3);
        } catch (Exception $exception) {
            /** @var Stock $stock1 */
            $stock1 = $this->stockHandler->getStock(self::SINGLE_STOCK_NAME);

            $this->assertEquals(9, $this->stockHandler->getAvailableCapacity($stock1));
            $this->assertEquals(1, $this->stockHandler->countActualProduct($stock1));
            $this->assertEquals("Wasn't enough products in stocks. Couldn't remove 1 products", $exception->getMessage());
        }

    }

    public function testRemoveMultiProductFromMultiStockSuccessful()
    {
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_1, 10, 'Test');
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_2, 10, 'Test');
        $this->stockHandler->addProduct($this->testProduct1, 9);
        $this->stockHandler->addProduct($this->testProduct2, 4);

        $this->stockHandler->removeProduct($this->testProduct1, 8);
        $this->stockHandler->removeProduct($this->testProduct2, 3);

        /** @var Stock $stock1 */
        $stock1 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_1);
        /** @var Stock $stock2 */
        $stock2 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_2);

        $this->assertEquals(9, $this->stockHandler->getAvailableCapacity($stock1));
        $this->assertEquals(1, $this->stockHandler->countActualProduct($stock1));
        $this->assertEquals(9, $this->stockHandler->getAvailableCapacity($stock2));
        $this->assertEquals(1, $this->stockHandler->countActualProduct($stock2));
    }

    public function testRemoveMultiProductFromMultiStockFail()
    {
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_1, 10, 'Test');
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_2, 10, 'Test');
        $this->stockHandler->addProduct($this->testProduct1, 7);
        $this->stockHandler->addProduct($this->testProduct2, 7);

        try {
            $this->stockHandler->removeProduct($this->testProduct1, 6);
            $this->stockHandler->removeProduct($this->testProduct2, 9);
        } catch (Exception $exception) {
            /** @var Stock $stock1 */
            $stock1 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_1);
            /** @var Stock $stock2 */
            $stock2 = $this->stockHandler->getStock(self::MULTI_STOCK_NAME_2);

            $this->assertEquals(9, $this->stockHandler->getAvailableCapacity($stock1));
            $this->assertEquals(1, $this->stockHandler->countActualProduct($stock1));
            $this->assertEquals(10, $this->stockHandler->getAvailableCapacity($stock2));
            $this->assertEquals(0, $this->stockHandler->countActualProduct($stock2));
            $this->assertEquals("Wasn't enough products in stocks. Couldn't remove 2 products", $exception->getMessage());
        }
    }

    public function testGetSingleStock()
    {
        $this->stockHandler->createStock(self::SINGLE_STOCK_NAME, 10, 'Test');
        $this->stockHandler->addProduct($this->testProduct1, 4);
        $this->stockHandler->addProduct($this->testProduct2, 4);

        /** @var Stock $stock */
        $stock = $this->stockHandler->getStock(self::SINGLE_STOCK_NAME);

        $productFF = $stock->getProductCollection()[0];
        $productFS = $stock->getProductCollection()[1];


        $this->assertEquals(self::SINGLE_STOCK_NAME, $stock->getName());
        $this->assertEquals(10, $stock->getCapacity());
        $this->assertEquals('Test', $stock->getAddress());
        $this->assertEquals($this->testProduct1->getId(), $productFF['product']);
        $this->assertEquals(4, $productFF['qty']);
        $this->assertEquals($this->testProduct2->getId(), $productFS['product']);
        $this->assertEquals(4, $productFS['qty']);
    }

    public function testGetMultiStock()
    {
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_1, 10, 'Test');
        $this->stockHandler->createStock(self::MULTI_STOCK_NAME_2, 10, 'Test');
        $this->stockHandler->addProduct($this->testProduct1, 7);
        $this->stockHandler->addProduct($this->testProduct2, 7);

        $stocks = $this->stockHandler->getStocks();

        /** @var Stock $stock1 */
        $stock1 = $stocks[0];
        /** @var Stock $stock2 */
        $stock2 = $stocks[1];

        $productFF = $stock1->getProductCollection()[0];
        $productFS = $stock1->getProductCollection()[1];
        $productSF = $stock2->getProductCollection()[0];


        $this->assertEquals(self::MULTI_STOCK_NAME_1, $stock1->getName());
        $this->assertEquals(10, $stock1->getCapacity());
        $this->assertEquals('Test', $stock1->getAddress());
        $this->assertEquals($this->testProduct1->getId(), $productFF['product']);
        $this->assertEquals(7, $productFF['qty']);
        $this->assertEquals($this->testProduct2->getId(), $productFS['product']);
        $this->assertEquals(3, $productFS['qty']);


        $this->assertEquals(self::MULTI_STOCK_NAME_2, $stock2->getName());
        $this->assertEquals(10, $stock2->getCapacity());
        $this->assertEquals('Test', $stock2->getAddress());
        $this->assertEquals($this->testProduct2->getId(), $productSF['product']);
        $this->assertEquals(4, $productSF['qty']);
    }
}